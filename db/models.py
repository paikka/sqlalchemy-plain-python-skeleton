from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.sql import func
from . guid import GUID
from . meta import Base
import uuid


class CoreModel(Base):

    __abstract__ = True

    id = Column(GUID, primary_key=True, default=uuid.uuid4)
    ts_created = Column(DateTime(), nullable=False, server_default=func.now())

class Category(CoreModel):
    __tablename__ = "category"
    
    name = Column(String, nullable=False)



